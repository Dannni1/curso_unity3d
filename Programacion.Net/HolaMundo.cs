//Al principio se ponen las importaciones.
using System;

public class HolaMundo {
	
	static void Main(){
		Console.Beep();
		
		Console.WriteLine ("Hola mundo!");
		Console.WriteLine ("¿Qué tal?");
		Console.ReadKey ();
		
		Console.WriteLine ("Pues bien");
		Console.Beep();
		
		//tipo nombre ;
		byte unNumero;
		unNumero = 10; // byte de 0 a 255
		Console.WriteLine("El byte es " + unNumero);
	
		char unCaracter; //O bien 1 ó 2 bytes
		unCaracter = 'A'; //comillas simples para caracteres
						 //comillas DOBLES para cadena caracteres
		Console.WriteLine("El caracter es " + unCaracter);
		//unCaracter también es número
		unCaracter = (char) (65+4);//un char es un número, pero para hacer
								//la conveersión hay que hacer "casting"
		Console.WriteLine ("El caracter es " + unCaracter);
	}
}