/*Estos son los tipos primitivos de C#
Igual que en otros lenguajes de programacion*/
using System;
public class TiposDeDatos {
	public static void Main ()
	{
		byte unByte = 255; //Numentero 0 y 255, 8bits, 2^8
		char unCaracter = 'A'; // Un caracter tb es un num
		int numEntero = 1000; //N?mero entre -2.000.000.000 y +2.000.000.000
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Byte: " + unByte + " char:" + unCaracter);
		Console.WriteLine("Un Char ocupa " + sizeof(char) + numEntero);
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Un Char ocupa " + sizeof(int) + numEntero);
		numEntero = 1000000000; //mil millones
		Console.WriteLine ("Ahora el entero vale " +numEntero);
		numEntero = 2000000000; //2 mil millones max

		Console.WriteLine ("Ahora el entero vale " + numEntero);
		//para guardar n?meros m?s largos
		long enteroLargo = 5000000000; // 8 bytes
		Console.WriteLine("El entero vale " +enteroLargo);
		float numDecimal = 1.23456789f;
		Console.WriteLine("Num decimal float vale " + numDecimal);
		//Para mayor precisi?n: double
		double numDoblePrecision = 12345.6789123456789;
		Console.WriteLine("Num decimal doble vale " + numDoblePrecision);
		
		// Para guardar Si/No, Verdad/Falso, Cero/Uno...
		bool variableBooleana = true;
		Console.WriteLine ("variableBooleana vale " + variableBooleana);
		variableBooleana = numDecimal > 1000;
		Console.WriteLine ("variableBooleana vale " + variableBooleana);
		
		string cadenaDeTexto = "Pues eso, una cadena de texto";
		Console.WriteLine(cadenaDeTexto);
		Console.WriteLine(cadenaDeTexto + "que " + " permite concatencacion");
	
}
}