using System;
public class estructurasDeControl {
	public static void Main()
	{
		EjemploIfSimple();
		EjemploIfCompliado();
		EjercicioSuma();
		EjemploIfConsecutivos();
	}
		
	
	
	static void EjemploIfSimple()
	{
		if(true) Console.WriteLine ("Pues sí");
		//if(false) Console.WriteLine ("Pues no");

		bool oSioNo =true;
		if (oSioNo) Console.WriteLine("Pues también sí");
		if (5==5) Console.WriteLine ("Pues 5==5");
		//if (5<4) Console.WriteLine ("Esto tampoco se muestra");
	}
	
	static void EjemploIfCompliado()
	{
		if (4>=7) Console.WriteLine ("4 >= 7"); else Console.WriteLine ("4 < 7");
		if ("Hola" == "hola") Console.WriteLine ("Son dist");
		Console.WriteLine ("Son iguales");
	}
		
	static void EjercicioSuma()
	{
	

	int resultado = 50;
	//Se suma las 3 combinaciones (A+B, B+C y A+C) y
	//que el programa diga cual es el resultado.
	//Consola debe mostrar:
	//A + B es igual resultado:
	//A + C es distinto que resultado:
	//B + C es distinto que reusltado:
	
	string numA = "20", numB = "30" , numC = "40";
	
	int intNumA = Int32.Parse(numA);
	int intNumB = Int32.Parse(numB);
	int intNumC = Int32.Parse(numC);
	
	int resultadoAB = intNumA + intNumA;
	int resultadoAC = intNumA + intNumC;
	int resultadoBC = intNumB + intNumC;

		Console.WriteLine ((resultado) + intNumA + intNumB);

	if (intNumA + intNumB == resultado)
		Console.WriteLine ("A + B es  igual a resultado");
	else
		Console.WriteLine ( "A + B es  distinto de resultado");
	
	if (intNumA + intNumC == resultado)
		Console.WriteLine (" A + C es igual a resultado");
	else
		Console.WriteLine ( "A + B es  distinto de resultado");
	
	if (intNumB + intNumC == resultado)
		Console.WriteLine (" B + C es igual a resultado");
	else
		Console.WriteLine ( "B + C es  distinto de resultado");
	}

	static void EjemploIfConsecutivos()
	{
		String Opcion = Console.ReadKey().Key.ToString();
		Console.WriteLine("Introduzca una opción: ");
		Console.WriteLine("1 - opción: primera ");
		Console.WriteLine("2 - opción: segunda  ");
		Console.WriteLine("3 - opción: tercera  ");
		Console.WriteLine("(*) - otra opción. ");
		
		if (Opcion == "1")
			Console.WriteLine("Has elegido la primera ");
		else if (Opcion == "2")
			Console.WriteLine("Has elegido la segunda ");
	}
	
	//El bucle while puede crear cualquier tipo de bucle
	//Puede que nunca se ejecute (si el booleano (contradición) es falsa
	//de entrada ... O puede que sea un bucle infinito si la condición es siempre true
	//O un bucle normal con una duración determinada
	static void EjemploBucleWhile ()
	{
	Console.WriteLine ("Antes del bucle");
	while(false)
	Console.WriteLine("Instruccion repetida");
	int contador = 0;
	
	
	while(contador < 10)
	{
	Console.WriteLine ("Contador = " + contador);
	contador = contador + 1;
	}
	bool siContinuar = true;
	while (siContinuar)
	{
	Console.WriteLine ("Estamos en el bucle. ¿Desea salir?");
	string tecla = Console.ReadKey().Key.ToString();
	}
	Console.WriteLine("Fin del bucle");
	}
	
	static void EjemploBucleFor (){
		//Un bucle for es un while que se suele usar
		//con un contador; for (<inicializacion>;
		//<condición> ; <imcremento>);
		//instrucción o bién un {bloque que se repite}
		for (int contador = 0 ; contador < 10 ; contador = contador + 1) ;
		{
			Console.WriteLine("Contador = " + contador);
		}
	}
	
	static void EjemploBucleDoWhile(){
		//Es como el bucle while, pero se ejecuta sí o sí
		//al menos se va a ejecutar una vez
		do{
			Console.WriteLine("Al menos una vez");
		}while(false);
		bool siSalir = true;
		do{
			Console.WriteLine("Hasta que sea falsa");
			siSalir != siSalir;
		} while (!siSalir);
	}
	public void ArrayDeFloats(){
		float[] fuerzas = new float [5];
		for (int i; i< fuerzas.Length; i += 1){
			fuerzas[i] = 1.2345f + i;
		}
		Console.WriteLine("¿Qué fuerza quieres ver?");
		int pos = Int32.Parse(Console.ReadKey);
		Console.WriteLine("La fuerza núm " + pos + " es de: ");
		Console.WriteLine(fuerzas[pos]+ " newtons");

	}
}