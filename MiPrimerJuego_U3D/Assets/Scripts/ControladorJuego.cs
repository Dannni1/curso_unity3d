﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{


    public GameObject[] prefabEnemigos;
    public AparicionEnemigos[] aparicionEnem;


    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public GameObject[] InstanciarEnemigo;

    private float tiempoInicio;
    private int enemActual;



    public int puntosAnt; //Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Instantiate(prefabEnemigos[Random.Range(0, prefabEnemigos.Length)]);
        tiempoInicio = Time.time; // momento actual del momento inicial
        enemActual = 0;



    }

    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "vidas: " + this.vidas;
        textoPuntos.GetComponent<Text>().text = "puntos: " + this.puntos;
        // Para saber si un Enem tiene que aparecer, tenemos que saber
        // cuánto tiempo ha pasado desde el inicio del nivel hasta
        //el momento actual (frame actual) y si es superior al tiempo
        //configurado en la aparción del elemento
        float tiempoActual = Time.time - tiempoInicio;
        if (aparicionEnem[0].tiempoInicio > tiempoActual) {

            if (!aparicionEnem[enemActual].yaHaAparecido) {
                this.InstanciarEnemigo();
                aparicionEnem[enemActual].yaHaAparecido = true;
            }
        }

    }

    public void InstanciarEnemigo()
        {
        int numEnemigo = Random.Range(0, prefabEnemigos.Length);
        GameObject.Instantiate(prefabEnemigos[numEnemigo]);
        }

    public void CuandoCapturamosEnemigo()
    {
        // Estamos asignando un nuevo valor a la puntuación,
        // que es los puntos actuales + 10 ptos.
        this.puntos = this.puntos + 10;     // this.puntos  += 10;
        GameObject.Instantiate(prefabEnemigos[Random.Range(0, prefabEnemigos.Length)]);
    }
    public void CuandoPerdemosEnemigo()
    {
        this.vidas = this.vidas - 1;        // this.vidas -= 1;   this.vidas--;
        GameObject.Instantiate(prefabEnemigos[Random.Range(0, prefabEnemigos.Length)]);
    }


        
        /*public void FinDelJuego()
        {
        bool gameOver = false;
        gameOver = -1;
        if (gameOver == false)
            {
                gameOver = true;
                Debug.Log("GAME OVER");
            }
        }*/

}

