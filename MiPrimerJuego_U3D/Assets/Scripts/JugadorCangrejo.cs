﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorCangrejo : MonoBehaviour
{
    public float velocidad;
    // Update is called once per frame
 
    void Update()
    {

        //cuando se pulsa izquierda
        if (Input.GetKey(KeyCode.A))

        //Creamos el vector de movimiento, a partir del calculo
        //influye la velocidad(40), el vector hacia la izquierda (-1,0,0)
        //Para que se mueva -40 unid. por segundo en vez de por cada frame,
        //multipliacmos por el incremento del tiempo (aprox. 0.2 seg, 50FPS) (0.8, 0, 0)

        {
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;

            //si la posición en el eje X es menor que -10 (margen izq)
            this.GetComponent<Transform>().Translate(vectorMov);
            if (this.GetComponent<Transform>().position.x < -10)
            {
                this.GetComponent<Transform>().position = new Vector3(-10, -5.5f, 0);
            }
        }

        //cuando se pulsa derecha
        if (Input.GetKey(KeyCode.D))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
            if (this.GetComponent<Transform>().position.x > 10)
            {
                this.GetComponent<Transform>().position = new Vector3(+10, -5.5f, 0);
            }
        }
    }
}
