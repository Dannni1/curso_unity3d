﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemigoLata : MonoBehaviour

{
    public float velocidad;
    GameObject jugador;
    GameObject controladorJuego;
    void Start()
    {
        float posInicioX = Random.Range(-10, 10);
        this.transform.position = new Vector3(posInicioX, 10, 0);
        // Buscamos un GameObject por su nombre, sólo hacer en Start ()
        jugador = GameObject.Find("Jugador-Caballito");
        controladorJuego = GameObject.Find("Controlador-Juego");
    }


    // Update is called once per frame
    void Update()
    {
        {
            Vector3 vectorMov = velocidad * Vector3.down * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);
            if (this.GetComponent<Transform>().position.y < -5)
            {
                this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -5.5f, 0);
                if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
               && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
                {
                    this.controladorJuego.GetComponent<ControladorJuego>()
                        .CuandoCapturamosEnemigo();
                    Destroy(this.gameObject);
                }
                else
                {
                    this.controladorJuego.GetComponent<ControladorJuego>()
                    //controladorJuego
                         .GetComponent<ControladorJuego>()
                        .CuandoPerdemosEnemigo();
                    Destroy(this.gameObject);

                }
            }


            /*if (this.GetComponent<Transform>().position.y < -5)
            {
                this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -5.5f, 0);

                Destroy(this.gameObject);
            }

            else
            {
                GameObject.Find("Controlador-Juego")
                    .GetComponent<ControladorJuego>()
                    .vidas -= 1;

            }*/

        }
    }
}